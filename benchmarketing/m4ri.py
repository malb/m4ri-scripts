#!/usr/bin/env sage
from sage.all import *
from benchmarketing import Benchmarketing, BenchFunction
from optparse import OptionParser

def bench_multiplication_sage(n):
    K = GF(2)
    A = random_matrix(K, n, n)
    B = random_matrix(K, n, n)
    ct = cputime()
    _ = A*B
    ct = cputime(ct)
    return ct

def bench_multiplication_magma(n):
    _ = magma.eval("K := GF(2)")
    A = magma("RandomMatrix(K, %d, %d)"%(n,n))
    B = magma("RandomMatrix(K, %d, %d)"%(n,n))
    ct = magma.cputime()
    _ = A*B
    ct = magma.cputime(ct)
    return ct

def bench_elimination_sage(n):
    K = GF(2)
    A = random_matrix(K, n, n)
    ct = cputime()
    _ = A.echelon_form()
    ct = cputime(ct)
    return ct

def bench_elimination_magma(n):
    _ = magma.eval("K := GF(2)")
    A = magma("RandomMatrix(K, %d, %d)"%(n,n))
    ct = magma.cputime()
    _ = A.EchelonForm()
    ct = magma.cputime(ct)
    return ct

def main():
    parser = OptionParser(usage="""\
Benchmarketing M4RI and Magma.
""")
    parser.add_option('--max-n',
                      type='int', action='store',
                      help='matrix maximum dimension', default=3.2*10**4)
    parser.add_option('--min-n',
                      type='int', action='store',
                      help='matrix minimum dimension.', default=1000)
    parser.add_option('--step-n',
                      type='int', action='store',
                      help='matrix dimension step size.', default=1000)
    parser.add_option('--smooth-window',
                      type='int', action='store',
                      help='Smooth window size.', default=0)
    parser.add_option("-a", "--all",
                  action="store_true", dest="all", default=False,
                  help="all multiples of 1000 up to max n")

    opts, args = parser.parse_args()

    try:
        function = args[0]
    except IndexError:
        parser.print_help()
        return

    set_verbose(1)

    if not opts.all:
        X = []
        for n in (10**4, 2**14, 2*10**4, 3.2*10**4):
            if n <= opts.max_n:
                X.append(n)
    else:
        X = range(opts.min_n, opts.max_n+1, opts.step_n)
                
    if function == "multiplication":
        B = Benchmarketing("Multiplication", X, smooth_window=opts.smooth_window)
        B(BenchFunction("Magma", bench_multiplication_magma),
          BenchFunction("Sage",  bench_multiplication_sage))
        return

    elif function == "elimination":
        B = Benchmarketing("Elimination", X, smooth_window=opts.smooth_window)
        B(BenchFunction("Magma", bench_elimination_magma),
          BenchFunction("Sage",  bench_elimination_sage))
        return

    parser.print_help()
    return

if __name__ == '__main__':
    main()
