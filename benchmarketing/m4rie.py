#!/usr/bin/env sage
from sage.all import *
from benchmarketing import Benchmarketing2D, Benchmarketing, BenchFunction
from optparse import OptionParser

def bench_multiplication_sage_2d(k,n):
    K = GF(2**k,'a')
    A = random_matrix(K, n, n)
    B = random_matrix(K, n, n)
    ct = cputime()
    _ = A*B
    ct = cputime(ct)
    return ct

def bench_multiplication_magma_2d(k,n):
    _ = magma.eval("K<a> := GF(2^%d)"%k)
    A = magma("RandomMatrix(K, %d, %d)"%(n,n))
    B = magma("RandomMatrix(K, %d, %d)"%(n,n))
    ct = magma.cputime()
    _ = A*B
    ct = magma.cputime(ct)
    return ct

def bench_elimination_sage_2d(k,n):
    K = GF(2**k,'a')
    A = random_matrix(K, n, n)

    ct = cputime()
    _ = A.echelon_form()
    ct = cputime(ct)
    
    return ct

def bench_elimination_magma_2d(k,n):
    _ = magma.eval("K<a> := GF(2^%d)"%k)
    A = magma("RandomMatrix(K, %d, %d)"%(n,n))
    ct = magma.cputime()
    _ = A.EchelonForm()
    ct = magma.cputime(ct)
    return ct

def main():
    parser = OptionParser(usage="""\
Benchmarketing M4RIE and Magma.

Supported functions are:

  multiplication-2d  - 2D heat map for multiplication
  elimination-2d     - 2D heat map for elimination
  multiplication-n   - multiplication comparison for dimension n
  elimination-n      - elimination comparison for d
  multiplication-e   - multiplication comparison for extension degree e
  elimination-e      - elimination comparison for extension degree e
""")
    parser.add_option('--min-e',
                      type='int', action='store',
                      help='finite field minimum exponent.', default=2)
    parser.add_option('--max-e',
                      type='int', action='store',
                      help='finite field maximum exponent.', default=10)
    parser.add_option('--min-n',
                      type='int', action='store',
                      help='matrix minimum dimension.', default=1000)
    parser.add_option('--max-n',
                      type='int', action='store',
                      help='matrix maximum dimension.', default=5000)
    parser.add_option('--step-n',
                      type='int', action='store',
                      help='matrix dimension step size.', default=100)
    parser.add_option('-n','--n',
                      type='int', action='store',
                      help='matrix dimension.', default=2000)
    parser.add_option('-e','--e',
                      type='int', action='store',
                      help='extension degree.', default=2)
    parser.add_option('--smooth-window',
                      type='int', action='store',
                      help='Smooth window size.', default=0)

    opts, args = parser.parse_args()

    try:
        function = args[0]
    except IndexError:
        parser.print_help()
        return

    set_verbose(1)

    if function.endswith("-2d"):
        X = range(opts.min_e, opts.max_e+1)
        Y = range(opts.min_n, opts.max_n+1,opts.step_n)

        if function.startswith("multiplication"):
            B2D = Benchmarketing2D("Multiplication", X, Y, smooth_window=opts.smooth_window)
            B2D(BenchFunction("Magma", bench_multiplication_magma_2d),
                BenchFunction("Sage",  bench_multiplication_sage_2d))
            return

        elif function.startswith("elimination"):
            B2D = Benchmarketing2D("Elimination", X, Y, smooth_window=opts.smooth_window)
            B2D(BenchFunction("Magma", bench_elimination_magma_2d),
                BenchFunction("Sage",  bench_elimination_sage_2d))
            return


    if function.endswith("-n"):
        X = range(opts.min_e, opts.max_e+1)
        if function.startswith("multiplication"):
            B = Benchmarketing("Multiplication at Dimension %d"%(opts.n,), X, smooth_window=opts.smooth_window)
            B(BenchFunction("Magma", lambda x: bench_multiplication_magma_2d(x, opts.n)),
              BenchFunction("Sage",  lambda x: bench_multiplication_sage_2d(x, opts.n)))
            return

        elif function.startswith("elimination"):
            B = Benchmarketing("Elimination at Dimension %d"%(opts.n,), X, smooth_window=opts.smooth_window)
            B(BenchFunction("Magma", lambda x: bench_elimination_magma_2d(x, opts.n)),
              BenchFunction("Sage",  lambda x: bench_elimination_sage_2d(x, opts.n)))
            return

    if function.endswith("-e"):
        X = range(opts.min_n, opts.max_n+1, opts.step_n)
        if function.startswith("multiplication"):
            B = Benchmarketing("Multiplication at Degree %d"%(opts.e,), X, smooth_window=opts.smooth_window)
            B(BenchFunction("Magma", lambda x: bench_multiplication_magma_2d(opts.e, x)),
              BenchFunction("Sage",  lambda x: bench_multiplication_sage_2d(opts.e, x)))
            return

        elif function.startswith("elimination"):
            B = Benchmarketing("Elimination at Degree %d"%(opts.e,), X, smooth_window=opts.smooth_window)
            B(BenchFunction("Magma", lambda x: bench_elimination_magma_2d(opts.e, x)),
              BenchFunction("Sage",  lambda x: bench_elimination_sage_2d(opts.e, x)))
            return

    parser.print_help()
    return

if __name__ == '__main__':
    main()
