# -*- coding: utf-8 -*-

from sage.all import *

class BenchFunction:
    """
    Essentially a function and a name
    """
    def __init__(self, name, f):
        """
        EXAMPLE::

            sage: f = BenchFunction("foo",lambda x,y: x*y)
            sage: f
            foo
            sage: f(2,3)
            6
        """
        self._name = name
        self._f = f

    def __call__(self, *args, **kwds):
        """
        EXAMPLE::

            sage: f = BenchFunction("bar",lambda x,y: x*y + 1)
            sage: f(2,3)
            7
        """
        return self._f(*args, **kwds)

    def __repr__(self):
        """
        EXAMPLE::

            sage: f = BenchFunction("bar",lambda x,y: x*y + 1)
            sage: f
            bar
        """
        return self._name

import numpy

def smooth(x,window_len=11,window='hanning'):
    """smooth the data using a window with requested size.
    
    This method is based on the convolution of a scaled window with the signal.
    The signal is prepared by introducing reflected copies of the signal 
    (with the window size) in both ends so that transient parts are minimized
    in the begining and end part of the output signal.
    
    input:
        x: the input signal 
        window_len: the dimension of the smoothing window; should be an odd integer
        window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
            flat window will produce a moving average smoothing.

    output:
        the smoothed signal
        
    example:

    t=linspace(-2,2,0.1)
    x=sin(t)+randn(len(t))*0.1
    y=smooth(x)
    
    see also: 
    
    numpy.hanning, numpy.hamming, numpy.bartlett, numpy.blackman, numpy.convolve
    scipy.signal.lfilter
 
    TODO: the window parameter could be the window itself if an array instead of a string   
    """

    if x.ndim != 1:
        raise ValueError, "smooth only accepts 1 dimension arrays."

    if x.size < window_len:
        raise ValueError, "Input vector needs to be bigger than window size."


    if window_len<3:
        return x


    if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise ValueError, "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'"


    s=numpy.r_[2*x[0]-x[window_len:1:-1],x,2*x[-1]-x[-1:-window_len:-1]]
    #print(len(s))
    if window == 'flat': #moving average
        w=ones(window_len,'d')
    else:
        w=eval('numpy.'+window+'(window_len)')

    y=numpy.convolve(w/w.sum(),s,mode='same')
    return y[window_len-1:-window_len+1]

class Benchmarketing:
    def __init__(self, title, X, smooth_window=None, **kwds):
        self._title = title
        self._X = list(X)
        self._smooth_window = smooth_window

    @staticmethod
    def run_benchmark(f, args=None):
        avg = lambda l: sum(l)/len(l)
        if args is None: args = []
        timings = []
        experiment_name = "%s:%s"%(f,args)
        timings.append(f(*args))
        if get_verbose() >= 1:
            sys.stdout.write("%20s: i = %4d, t = %8.4f s, δ = %6.3f%%    \r"%(experiment_name, 0, avg(timings), 100))
            sys.stdout.flush()
        prev = timings[0]

        set_random_seed(1337)

        for i in range(1000):
            v = f(*args)

            try:    
                delta = abs(avg(timings)/avg(timings + [v]) - 1.0)
            except ZeroDivisionError:
                delta = 1.0

            timings.append( v )

            if get_verbose() >= 1:
                sys.stdout.write("%20s: i = %4d, t = %8.4f s, δ = %6.3f%%    \r"%(experiment_name, i+1, avg(timings), delta))
                sys.stdout.flush()

            if (delta < 0.001 and sum(timings) > 1.0) or sum(timings) > 3600.0:
                break

        if get_verbose() >= 1:
            sys.stdout.write("\n")
            sys.stdout.flush()
        return avg(timings)

    def __call__(self, a, b):
        self.R = Matrix(RR, len(self._X), 2)
        for i,x in enumerate(self._X):
            av = Benchmarketing.run_benchmark(a,(x,))
            bv = Benchmarketing.run_benchmark(b,(x,))
            self.R[i,0] = av
            self.R[i,1] = bv
            if not bv:
                bv = 0.0000001
            if get_verbose() >= 1:
                print
            #print "%10s %10.4f %10.4f"%(x,av,bv)
            self._plot(str(a),str(b), length=i+1)

        print
        print "          %10s%10s"%(a,b)
        for i,x in enumerate(self._X):
            print "%10s %10.4f %10.4f"%(x,self.R[i,0],self.R[i,1])
           
        save(self,'benchmarketing-%s-%s-%s.sobj'%(self._title, a, b))

    def _plot(self, a_name, b_name, length=None):
        if length is None:
            length = len(self._X)

        A,B = self.R.columns()
        A = A[:length]
        B = B[:length]

        if len(A) > 12 and self._smooth_window:
            A = list(smooth(numpy.array(A), self._smooth_window))

        if len(B) > 12 and self._smooth_window:
            B = list(smooth(numpy.array(B), self._smooth_window))

        import pylab

        pylab.clf() # clear the figure first
        pylab.figure(1)

        rel = []
        for i in range(length):
            if B[i]:
                rel.append(log_b(A[i]/B[i],2.0))
            else:
                rel.append(0.0)
        if len(rel) > 12 and self._smooth_window:
            rel = list(smooth(numpy.array(rel),self._smooth_window))

        pylab.plot(self._X[:length], A[:length],label=a_name, color="red")
        pylab.plot(self._X[:length], B[:length],label=b_name, color="blue")
        pylab.plot(self._X[:1], A[:1],label="%s/%s"%(a_name,b_name), color="darkgrey", alpha=0.5, lw=2.0)
        pylab.legend(loc=2) # print the legend
        pylab.ylabel("execution time $t$") # label the axes

        pylab.twinx()
        pylab.plot(self._X[:length], rel,label="%s/%s"%(a_name,b_name), color="darkgrey", alpha=0.5, lw=2.0)
        # pylab.legend(loc=4) # print the legend
        pylab.ylabel("$log_2$ relation") # label the axes

        pylab.title(self._title)
        if length < 8:
            pylab.xticks(self._X[:length])
        else:
            pylab.xticks(self._X[:length][0::ceil(length/8.0)])

        pylab.savefig('benchmarketing-%s-%s-%s.png'%(self._title, a_name, b_name),dpi=96) # fire!
        pylab.savefig('benchmarketing-%s-%s-%s.pdf'%(self._title, a_name, b_name),dpi=96) # fire!

        fh = open('benchmarketing-%s-%s-%s.txt'%(self._title, a_name, b_name),"w")
        fh.write("          %10s%10s\n"%(a_name,b_name))
        for i,x in enumerate(self._X[:length]):
            fh.write("%10s %10.4f %10.4f\n"%(x,self.R[i,0],self.R[i,1]))
        fh.close()

class Benchmarketing2D(Benchmarketing):
    """
    Plots a 2D Win/Loose Heatmap.
    """
    def __init__(self, title, X, Y, **kwds):
        self._title = title
        self._X = list(X)
        self._Y = list(Y)
        
    def __call__(self, a, b):
        """
        Iterates over all x,y combinations and evaluates the functions
        ``a`` and ``b`` on the ``x,y`` pairs. Intermediate timings are
        plotted after each line (X) is computed.
        """
        self.R = Matrix(RR,len((self._X)),len(self._Y))
        self.R = self.R.apply_map(lambda x: RR(1.0))
        for i,x in enumerate(self._X):
            for j,y in enumerate(self._Y):
                av = Benchmarketing.run_benchmark(a,(x,y))
                bv = Benchmarketing.run_benchmark(b,(x,y))
                if not bv:
                    bv = 0.0000001
                self.R[i,j] = av/bv
                if self.R[i,j] == 0.0:
                    self.R[i,j] = 1.0
            self._plot(str(a),str(b))
        print self.R.str()
        save( (self.R,self._title,a,b), 'benchmarketing2d-%s-%s-%s.sobj'%(self._title,str(a),str(b)) )

        return self._plot(str(a),str(b))

    def _plot(self, a_name, b_name, scale="log", what="Multiplication", results=None):

        if results is None:
            results = self.R

        if scale == "log":
            results = results.apply_map(lambda x: log_b(x,2.0))

        import pylab

        Z = pylab.array(results)
        pylab.clf()
        vmax = max([abs(e) for e in results.list()])
        vmin = -max([abs(e) for e in results.list()])
        c = pylab.pcolor(Z, cmap=pylab.cm.RdBu, vmin=vmin, vmax=vmax)

        pylab.title("%s: %s vs. %s"%(self._title, a_name,b_name))
        pylab.colorbar()

        pylab.xticks(range(len(self._Y))[1::len(self._Y)/5])
        pylab.yticks(range(len(self._X))[1:])
        _ = c.axes.set_xticklabels(map(str,self._Y)[0::len(self._Y)/5])
        _ = c.axes.set_yticklabels(map(str,self._X)[0:])

        pylab.savefig('benchmarketing2d-%s-%s-%s.png'%(self._title,a_name,b_name),dpi=96)
        pylab.savefig('benchmarketing2d-%s-%s-%s.pdf'%(self._title,a_name,b_name),dpi=96)
        return c
