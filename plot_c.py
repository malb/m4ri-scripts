#!/usr/bin/env sage
from sage.all import *
from optparse import OptionParser

import numpy

def smooth(x,window_len=11,window='hanning'):
    """smooth the data using a window with requested size.

    This method is based on the convolution of a scaled window with the signal.
    The signal is prepared by introducing reflected copies of the signal
    (with the window size) in both ends so that transient parts are minimized
    in the begining and end part of the output signal.

    input:
        x: the input signal
        window_len: the dimension of the smoothing window; should be an odd integer
        window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
            flat window will produce a moving average smoothing.

    output:
        the smoothed signal

    example:

    t=linspace(-2,2,0.1)
    x=sin(t)+randn(len(t))*0.1
    y=smooth(x)

    see also:

    numpy.hanning, numpy.hamming, numpy.bartlett, numpy.blackman, numpy.convolve
    scipy.signal.lfilter

    TODO: the window parameter could be the window itself if an array instead of a string
    """

    if x.ndim != 1:
        raise ValueError, "smooth only accepts 1 dimension arrays."

    if x.size < window_len:
        raise ValueError, "Input vector needs to be bigger than window size."


    if window_len<3:
        return x


    if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise ValueError, "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'"


    s=numpy.r_[2*x[0]-x[window_len:1:-1],x,2*x[-1]-x[-1:-window_len:-1]]
    #print(len(s))
    if window == 'flat': #moving average
        w=ones(window_len,'d')
    else:
        w=eval('numpy.'+window+'(window_len)')

    y=numpy.convolve(w/w.sum(),s,mode='same')
    return y[window_len-1:-window_len+1]

def parse_file(fn):
    """
    """
    L = []
    for line in open(fn).readlines():
        if not "wall time:" in line:
            continue
        line = line.split(",")
        s = {}
        for entry in line:
            k,v = entry.split(":")
            k = k.strip()
            v = v.strip()
            try:
                v = eval(v)
            except NameError:
                pass
            s[k] = v
        L.append(s)
    return L


def filter_list(filter_str, L):
    L2 = []
    for entry in L:
        if eval(filter_str,{},entry):
            L2.append(entry)
    return L2

def split_list(split_entry, L):
    if split_entry is None:
        return {None:L}

    s = {}
    for entry in L:
        if entry[split_entry] in s:
            s[entry[split_entry]].append(entry)
        else:
            s[entry[split_entry]]  = [entry]
    return s

def plot(s, x_axis, y_axis, title='', smooth_window=0):
    colours = ['red','green','blue','orange']

    import pylab
    pylab.clf()
    pylab.figure(1)

    for i,(name,L) in enumerate(s.iteritems()):
        l = smooth(numpy.array([e[y_axis] for e in L]), smooth_window)
        pylab.plot([e[x_axis] for e in L], l, label=str(name), color=colours[i], linewidth=2)
        if len(L) < 8:
            pylab.xticks([e[x_axis] for e in L])
        else:
            pylab.xticks([e[x_axis] for e in L][0::ceil(len(L)/8.0)])

    pylab.legend(loc=2)
    pylab.xlabel(x_axis)
    pylab.ylabel(y_axis)
    pylab.title(title)

    pylab.savefig('benchmarketing-%s.png'%(title.lower().replace(" ","_"),),dpi=96) # fire!
    pylab.savefig('benchmarketing-%s.pdf'%(title.lower().replace(" ","_"),),dpi=96) # fire!

def main():
    parser = OptionParser(usage="""\
Plot outputs of benchmarking C files shipped with M4RI(E).""")
    parser.add_option('--filter',
                      type='string', action='store',
                      help='only use entries satisfying this relation.', default='True')
    parser.add_option('--x-axis',
                      type='string', action='store',
                      help='field used for the x axis.', default='m')
    parser.add_option('--y-axis',
                      type='string', action='store',
                      help='field used for the y axis.', default='wall time')
    parser.add_option('--split',
                      type='string', action='store',
                      help='split curves using this field.', default=None)
    parser.add_option('--title',
                      type='string', action='store',
                      help='title of plot.', default='')
    parser.add_option('--smooth-window',
                      type='int', action='store',
                      help='Smooth window size.', default=0)

    opts, args = parser.parse_args()

    if len(args) != 1:
        parser.print_help()
        return

    L = parse_file(args[0])
    L = filter_list(opts.filter, L)
    L = split_list(opts.split, L)
    plot(L, opts.x_axis, opts.y_axis, opts.title, smooth_window=opts.smooth_window)

if __name__ == '__main__':
    main()
