from sage.all import *
import sys

def runit(n, c=3):
    n = int(n)
    l = []
    print ' ',
    for i in xrange(c):
        _ = magma.eval("A:=RandomMatrix(GF(2),%d,%d)"%(n,n))
        t = magma.cputime()
        _ = magma.eval("r:=Rank(A)");
        l.append(magma.cputime(t))
        print "% 2.3f"%float(l[-1]),
        sys.stdout.flush()
    print "\n %d % 2.3f % 2.3f % 2.3f % 2.3f"%(int(n),min(l), sum(l)/c, sorted(l)[c/2+1], max(l))

runit(10**4)
runit(2**14)
runit(2*10**4)
runit(3.2*10**4)
runit(6.4*10**4)
