#!/usr/bin/python
import sys
import time

def get_snapshot(pid):
    s = open("/proc/%d/status"%pid).read()
    for line in s.splitlines():
        if line.startswith("VmSize"):
            return int(line[7:-3])

def max_memory_usage(pid):
    initial = get_snapshot(pid)
    maximal = -1
    try:
        while True:
            time.sleep(0.001)
	    n = get_snapshot(pid)
	    if n > maximal:
	        maximal = n
    except (KeyboardInterrupt, IOError):
        print maximal - initial
	return

if __name__ == "__main__":
    max_memory_usage(int(sys.argv[1]))
