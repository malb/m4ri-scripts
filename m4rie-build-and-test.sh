#!/bin/bash

# We expect M4RI an M4RIE to be available locally

M4RI=`ls m4ri-*.tar.gz | sed -e 's/\.tar.gz//'`
M4RIE=`ls m4rie-*.tar.gz | sed -e 's/\.tar.gz//'`

TMP_DIR="`pwd`/m4rie-build-and-test-temp"
PREFIX=$TMP_DIR/local

export LD_LIBRARY_PATH="$PREFIX/lib:$LD_LIBRARY_PATH"

echo "################################################################"
echo "### Using $M4RI and $M4RIE"
echo "################################################################"

function build {
    if [ -f "$1.done" ]; then
        return 0
    fi

    tar xvfz ../$1.tar.gz
    cd $1
    ./configure --prefix=$PREFIX $2

    if [ $? -ne 0 ]; then
        echo "$1: error configuring"
        exit 1
    fi

    make $MAKEOPTS

    if [ $? -ne 0 ]; then
        echo "$1: error building"
        exit 1
    fi

    make $MAKEOPTS check

    if [ $? -ne 0 ]; then
        echo "$1: make check failed"
        exit 1
    fi

    make $MAKEOPTS install
    if [ $? -ne 0 ]; then
        echo "$1: install failed"
        exit 1
    fi

    echo "################################################################"
    echo "### Success building & testing $1"
    echo "################################################################"
    cd ..

    touch $1.done
}


if [ ! -d $TMP_DIR ]; then
    mkdir $TMP_DIR
fi
if [ ! -d $TMP_DIR/local ]; then
    mkdir $TMP_DIR/local
fi
cd $TMP_DIR

build $M4RI 
build $M4RIE "--with-m4ri=$PREFIX"

cd ..
rm -rf $TMP_DIR


