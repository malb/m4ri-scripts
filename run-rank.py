from sage.all import *
import sys

lqup = sage.matrix.matrix_mod2_dense.lqup
def runit(n, c=3):
    l = []
    print ' ',
    for i in xrange(c):
        A = random_matrix(GF(2),n,n)
        t = cputime()
        _ = lqup(A)
        l.append(cputime(t))
        print "% 2.3f"%float(l[-1]),
        sys.stdout.flush()
    print "\n %d % 2.3f % 2.3f % 2.3f % 2.3f"%(int(n),min(l), sum(l)/c, sorted(l)[c/2+1], max(l))

runit(10**4)
runit(2**14)
runit(2*10**4)
runit(3.2*10**4)
runit(6.4*10**4)
