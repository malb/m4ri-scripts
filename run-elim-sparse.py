from sage.all import *
import sys

N=10**4

def runit(n, c=3):
    l = []
    #print ' ',
    for i in xrange(c):
        A = random_matrix(GF(2),N,N,density=ZZ(n)/N)
        t = cputime()
        E = A.echelon_form('pluq')
        l.append(cputime(t))
        #print "% 2.3f"%float(l[-1]),
        sys.stdout.flush()
    print "%2d %6.3f %6.3f %6.3f %6.3f"%(int(n),min(l), sum(l)/c, sorted(l)[c/2+1], max(l))
    sys.stdout.flush()
    return sorted(l)[c/2+1]

L = []
for i in range(1,11):
    L.append((i,runit(i,c=7)))
