#!/usr/bin/python
import sys
import os
import commands
import datetime
from optparse import OptionParser
import mercurial
import mercurial.commands
import mercurial.hg
import tarfile, zipfile


import re

def do_release(name, release):
    dest = "%s-%s"%(name,release)
    cwd = os.getcwd()
    ui = mercurial.ui.ui()

    try:
        os.mkdir("release-script-tmp")
    except OSError:
        commands.getoutput("rm -rf release-script-tmp")
        os.mkdir("release-script-tmp")

    os.chdir("release-script-tmp")

    mercurial.commands.clone(ui,"http://bitbucket.org/malb/%s"%name)
    #repo = mercurial.hg.repository(ui, name)

    # I'm having trouble using the Python interface
    os.chdir(name)
    print commands.getoutput("hg archive ../%s"%dest)
    os.chdir("..")

    os.chdir(dest)
    print commands.getoutput("autoreconf --install")
    commands.getoutput("rm -r autom4te.cache")
    commands.getoutput("touch configure")
    commands.getoutput("touch src/config.h.in")

    for line in open("Makefile.am").readlines():
        m = re.match(".*-release 0\.0\.([0-9]{8}).*",line)
        if m:
            release_lib, = m.groups()
            if release_lib != release:
                print "###################################################################"
                print "WARNING '%s' in Makefile.am vs '%s' "%(release_lib, release)
                print "###################################################################"

    os.chdir("..")

    tar = tarfile.open("%s/%s.tar.gz"%(cwd,dest), "w:gz")
    tar.add(dest)
    tar.close()

    # For Windows users
    if name is "m4ri":
        commands.getoutput("rm %s/m4ri"%dest)
        commands.getoutput("mv %s/src %s/m4ri"%(dest,dest))

        cfg = open("%s/m4ri/m4ri_config.h"%dest,"w")
        cfg.write(
"""
#ifndef M4RI_M4RI_CONFIG_H
#define M4RI_M4RI_CONFIG_H

// Defines determined during configuration of m4ri.
#define __M4RI_HAVE_MM_MALLOC		0
#define __M4RI_HAVE_POSIX_MEMALIGN	0
#define __M4RI_HAVE_SSE2		0
#define __M4RI_HAVE_OPENMP		0
#define __M4RI_CPU_L1_CACHE		32768
#define __M4RI_CPU_L2_CACHE		2147483648
#define __M4RI_DEBUG_DUMP		(0 || 0)
#define __M4RI_DEBUG_MZD		0

// Helper macros.
#define __M4RI_USE_MM_MALLOC		(__M4RI_HAVE_MM_MALLOC && __M4RI_HAVE_SSE2)
#define __M4RI_USE_POSIX_MEMALIGN	(__M4RI_HAVE_POSIX_MEMALIGN && __M4RI_HAVE_SSE2)
#define __M4RI_DD_QUIET			(0 && !0)

#endif // M4RI_M4RI_CONFIG_H
"""
)
        cfg.close()

    commands.getoutput("zip -r %s/%s.zip %s"%(cwd,dest,dest))

    # cleanup
    os.chdir("..")
    commands.getoutput("rm -rf release-script-tmp")

def main():
    parser = OptionParser(usage="""\
Release M4RI(E).

The first argument must be either 'm4ri' or 'm4rie'.
""")
    parser.add_option('-r', '--release',
                      type='string', action='store',
                      help='Release version string.')
    opts, args = parser.parse_args()

    if opts.release is None:
        today = datetime.date.today()
        release = "%04d%02d%02d"%(today.year,today.month,today.day)
    else:
        release = opts.release

    try:
        name = args[0]
    except IndexError:
        parser.print_help()
        return

    do_release(name, release)

if __name__ == '__main__':
    main()
