N=8192

def runit(n, c=11, alg='pluq'):
    l = []
    for i in xrange(c):
        A = random_matrix(GF(2),N,N,density=ZZ(n)/N)
        if alg != 'magma':
            t = cputime()
            E = A.echelon_form(alg)
            l.append(cputime(t))
        else:
            Am = magma(A)
            t = magma.cputime()
            Em = Am.EchelonForm()
            l.append(magma.cputime(t))
        sys.stdout.flush()
    print "%2d %6.3f %6.3f %6.3f %6.3f"%(int(n),min(l), sum(l)/c, sorted(l)[c//2+1], max(l))
    return sorted(l)[c//2+1]

_magma = []
for i in range(1,51):
    _magma.append((i,runit(i, alg='magma')))
print

_m4ri = []
for i in range(1,51):
    _m4ri.append((i,runit(i, alg='m4ri')))
print

_pluq = []
for i in range(1,51):
    _pluq.append((i,runit(i,alg='pluq')))
print

import pylab
pylab.clf() # clear the figure first
pylab.figure(1)
# plot some data and add a legend
X = [x/2 for (x,y) in _m4ri]
pylab.plot(X,[y for (x,y) in _m4ri],label="M4RI")
pylab.plot(X,[y for (x,y) in _pluq],label="PLUQ")
pylab.plot(X,[y for (x,y) in _magma],label="Magma")
pylab.title("Sensitivity to Density $n=%d$"%(N,))
pylab.legend() # print the legend
pylab.ylabel("execution time $t$") # label the axes
pylab.xlabel("nonzero elements per row on average")
pylab.savefig('density-plot-%d.png'%(N,),dpi=100) # fire!
