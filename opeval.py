import commands

filtr = "/home/malb/SAGE/local/lib/libm4ri-0.0.20091101.so"
#filtr += " /home/malb/Projects/M4RI/Source/src/testsuite/bench_lqup"

def op_ratio(filtr=None):
    if filtr is not None:
        output = commands.getoutput("opreport -l " + filtr + " -e mzd_randomize,m4ri_coin_flip")
    else:
        output = commands.getoutput("opreport")


    data = output.splitlines()
    cpu = data[0]
    data = data[1:]
    while True:
        if "unit mask" in data[0]:
            event = data[0]
            data = data[1:]
            print event
        else:
            break

    data = data[1:]

    for line in data:
        line = [e for e in line.split() if e != '']
        if int(line[2]) <= 1000:
            break

        print "%30s %5.2f (%5.2f)"%(line[-1],float(line[0])/float(line[2]),float(line[1]))

op_ratio(filtr=filtr)
